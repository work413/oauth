/**
 * Module for the Controller.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import fetch from 'node-fetch'

/**
 * Encapsulates a controller.
 */
export class Controller {
  /**
   * Renders the index(home) page.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  index (req, res, next) {
    if (req.session.token) { // If the user is logged in
      const loggedIn = true

      res.render('account/index', { loggedIn })
    } else {
      res.render('account/index')
    }
  }

  /**
   * Renders the index (home) page with the user's GitLab profile data.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async profile (req, res, next) {
    try {
      const gitlabUser = await this.getGitlabUser(req.session.token)

      const userData = {
        name: gitlabUser.name,
        username: gitlabUser.username,
        id: gitlabUser.id,
        email: gitlabUser.email,
        avatar: gitlabUser.avatar_url,
        last_activity: gitlabUser.last_activity_on
      }

      res.removeHeader('Cross-Origin-Embedder-Policy') // Makes the avatar visible.
      res.render('account/profile', { userData })
    } catch (err) {
      const error = err
      error.status = parseInt(err.message)
      next(error)
    }
  }

  /**
   * Asks the user to authorize her/himself via GitLab using OAuth.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async oauth (req, res, next) {
    try {
      const clientId = process.env.CLIENT_ID
      const state = Math.random().toString(36).substring(2, 10)
      req.session.state = state

      // Sends authorization request GitLab's API.
      const gitlabResponse = await fetch(`https://gitlab.lnu.se/oauth/authorize?client_id=${clientId}&redirect_uri=http://localhost:8080/redirect&` +
      `response_type=code&state=${state}&scope=read_api+read_user`, {
        method: 'POST'
      })

      res.redirect(gitlabResponse.url) // Redirect to authorize via GitLab.
    } catch (err) {
      next(err)
    }
  }

  /**
   * Requests an authorization token from GitLab.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async requestToken (req, res, next) {
    try {
      if (req.query.state === req.session.state) { // Validate state
        const clientId = process.env.CLIENT_ID
        const clientSecret = process.env.CLIENT_SECRET

        // Requests tokens from GitLab's API.
        const accessTokenResponse = await fetch(`https://gitlab.lnu.se/oauth/token?client_id=${clientId}&client_secret=${clientSecret}&` +
        `code=${req.query.code}&grant_type=authorization_code&redirect_uri=http://localhost:8080/redirect`, {
          method: 'POST'
        }).then(response => response.json())

        if (accessTokenResponse.error) { // If the request failed.
          req.session.flash = { message: accessTokenResponse.error_description }
          res.redirect('..')
        } else {
          this.regenerateSession(req) // Regenerate the client session.
          this.setSessionTokens(req, accessTokenResponse.access_token, accessTokenResponse.refresh_token)
          res.redirect('/profile')
        }
      } else {
        req.session.flash = { message: 'Unvalid state' }
        res.redirect('/')
      }
    } catch (error) {
      next(error)
    }
  }

  /**
   * Revokes the authorization tokens from GitLab.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async revokeToken (req, res, next) {
    try {
      const clientId = process.env.CLIENT_ID
      const clientSecret = process.env.CLIENT_SECRET

      // Sends request to revoke tokens from GitLab.
      const revokeResponse = await fetch(`https://gitlab.lnu.se/oauth/revoke?client_id=${clientId}&client_secret=${clientSecret}&token=${req.session.token}`, {
        method: 'POST'
      }).then(response => response)

      if (revokeResponse.status === 401 || revokeResponse.status === 404) { // If the request failed.
        const err = new Error(revokeResponse.statusText)
        err.status = revokeResponse.status
        next(err)
      } else {
        this.regenerateSession(req) // Regenerate the client session.
        this.setSessionTokens(req, '', '') // Sets the session tokens to empty.
        req.session.flash = { message: 'Your access token from GitLab was revoked.' }
        res.redirect('/')
      }
    } catch (err) {
      next(err)
    }
  }

  /**
   * Regenerates the session with tokens and redirects back to the index page.
   *
   * @param {object} req - Express request object.
   */
  regenerateSession (req) {
    req.session.regenerate(function (err) {
      if (err) {
        console.log(err)
      }
    })
  }

  /**
   * Sets tokens on session.
   *
   * @param {object} req - Express request object.
   * @param {string} accessToken - The access token.
   * @param {string} refreshToken - The refresh token.
   */
  setSessionTokens (req, accessToken, refreshToken) {
    req.session.token = accessToken
    req.session.refresh = refreshToken
    req.session.save()
  }

  /**
   * Renders the 101 latest activities of the user from GitLab.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async activity (req, res, next) {
    try {
      const gitlabUser = await this.getGitlabUser(req.session.token)

      const events100 = await this.getEvents(gitlabUser.id, '100', '1', req.session.token) // Fetches the 100 lastest events from the first page.
      const event101 = await this.getEvents(gitlabUser.id, '1', '2', req.session.token) // Fetches 1 event from the second page.

      const allEvents = await Promise.all([events100, event101]).then(response => {
        const arr = [] // Will contain all objects created from the event data.

        response.forEach(elem => { // Loop through the two event responses.
          elem.forEach(event => { // Create an object from each event object in the responses.
            const obj = {
              action: event.action_name,
              created_at: event.created_at,
              target_title: event.target_title,
              target_type: event.target_type
            }
            arr.push(obj)
          })
        })

        return arr
      })

      const activity = { all: allEvents }

      res.render('account/activity', { activity })
    } catch (err) {
      const error = err
      error.status = parseInt(err.message)
      next(error)
    }
  }

  /**
   * Fetches current user from GitLab.
   *
   * @param {string} token - Authorization token.
   * @returns {object} - The GitLab user object.
   */
  async getGitlabUser (token) {
    // Sends a request to get the current user from GitLab.
    const gitlabUser = await fetch('https://gitlab.lnu.se/api/v4/user', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(response => response)

    if (gitlabUser.status === 401 || gitlabUser.status === 404) { // If the request failed.
      throw new Error(gitlabUser.status)
    } else {
      return gitlabUser.json()
    }
  }

  /**
   * Fetches user events from GitLab.
   *
   * @param {string} userId - ID of the GitLab user.
   * @param {string} perPage - Amount of events to show.
   * @param {string} page - The page index.
   * @param {string} token - Authorization token.
   * @returns {object} - The GitLab events object.
   */
  async getEvents (userId, perPage, page, token) {
    // Sends a request to get events of current user to GitLab.
    const events = await fetch(`https://gitlab.lnu.se/api/v4/users/${userId}/events?per_page=${perPage}&page=${page}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(response => response)

    if (events.status === 401 || events.status === 404) { // If the request failed.
      throw new Error(events.status)
    } else {
      return events.json()
    }
  }
}
