/**
 * The routes.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import createError from 'http-errors'
import { Controller } from '../controllers/controller.js'

export const router = express.Router()

const controller = new Controller()

router.get('/', (req, res, next) => controller.index(req, res, next))
router.get('/profile', (req, res, next) => controller.profile(req, res, next))
router.get('/submit', (req, res, next) => controller.oauth(req, res, next))
router.get('/redirect', (req, res, next) => controller.requestToken(req, res, next))
router.get('/activity', (req, res, next) => controller.activity(req, res, next))
router.get('/logout', (req, res, next) => controller.revokeToken(req, res, next))

// Catch 404 (ALWAYS keep this as the last route).
router.use('*', (req, res, next) => next(createError(404)))
