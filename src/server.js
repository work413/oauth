/**
 * The starting point of the application.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import hbs from 'express-hbs'
import session from 'express-session'
import helmet from 'helmet'
import logger from 'morgan'
import createError from 'http-errors'
import { dirname, join } from 'path'
import { fileURLToPath } from 'url'
import { router } from './routes/router.js'
import { connectDB } from './config/mongoose.js'

/**
 * The main function of the application.
 */
const main = async () => {
  await connectDB()

  const app = express()

  const directoryFullName = dirname(fileURLToPath(import.meta.url))

  // Set various HTTP headers to make the application little more secure (https://www.npmjs.com/package/helmet).
  app.use(helmet())

  app.use(
    helmet.contentSecurityPolicy({
      directives: {
        ...helmet.contentSecurityPolicy.getDefaultDirectives(),
        'script-src': ["'self'", 'https://gitlab.lnu.se/', 'cdn.jsdelivr.net'],
        'img-src': ["'self'", 'https://gitlab.lnu.se/', '*.gravatar.com', 'cdn.jsdelivr.net']
      }
    })
  )

  // Set up a morgan logger using the dev format for log entries.
  app.use(logger('dev'))

  // The view engine directories.
  app.engine('hbs', hbs.express4({
    defaultLayout: join(directoryFullName, 'views', 'default', 'default'),
    partialsDir: join(directoryFullName, 'views', 'partials')
  }))

  app.set('view engine', 'hbs')

  app.set('views', join(directoryFullName, 'views'))

  // Parse requests of the content type application/json.
  app.use(express.json())

  // Use the static files.
  app.use(express.static(join(directoryFullName, '..', 'static')))

  // Session information object.
  const sInfo = {
    name: process.env.SESSION_NAME,
    secret: process.env.SESSION_SECRET,
    saveUninitialized: false,
    resave: false,
    cookie: {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24,
      sameSite: 'lax'
    }
  }

  app.use(session(sInfo))

  // Handles flash messages.
  app.use((req, res, next) => {
    if (req.session.flash) {
      res.locals.flash = req.session.flash
      delete req.session.flash
    }

    next()
  })

  // Register routes.
  app.use('/', router)

  // Error handler.
  app.use(function (err, req, res, next) {
    if (err.status === 401) {
      return res.status(401).sendFile(join(directoryFullName, 'views', 'error', '401.html'))
    }

    if (err.status === 404) {
      return res.status(404).sendFile(join(directoryFullName, 'views', 'error', '404.html'))
    }

    if (!err.status) {
      const cause = err
      err = createError(500)
      err.cause = cause
    }

    if (req.app.get('env') !== 'development') {
      console.log('Running in production')
      app.set('trust proxy', 1)

      return res
        .status(err.status)
        .json({
          status: err.status,
          message: err.message
        })
    }

    // Development only!
    // Only providing detailed error in development.
    return res
      .status(err.status)
      .json({
        status: err.status,
        message: err.message,
        cause: err.cause ? JSON.stringify(err.cause, Object.getOwnPropertyNames(err.cause)) : undefined,
        stack: err.stack
      })
  })

  // Starts the HTTP server listening for connections.
  app.listen(process.env.PORT, () => {
    console.log(`Server running at http://localhost:${process.env.PORT}`)
    console.log('Press Ctrl-C to terminate...')
  })
}

main().catch(console.error)
