# OAuth

The ability to delegate access between systems is central in modern web applications. One popular standard för access delegation is OAuth (Open Authorization).

This application implements a three-legged OAuth2 access delegation between a server-side rendered web application (the consumer) and GitLab (the service provider).

The user can log in on the consumer application using her gitlab.lnu.se account and show some basic profile information, including at least the 101 most recent GitLab activities.
